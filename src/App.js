import React, { Component } from 'react';
import './App.css';
import moment from 'moment';
import Calendar from './components/Calendar'
import Scheduler from './components/Scheduler'
import SwitchUser from './components/SwitchUser'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDay: moment(),
      userId: 'user1'
    };
  }

  onSelectedDate = (day) => {
    this.setState({
      startDay: moment(day)
    });
  }

  onUserSwitch = (userId) => {
    this.setState({
      userId: userId
    });
  }

  render() {
    return (
      <div className="App">
        <header>
            <div id="logo">
                <span className="icon">date_range</span>
                <span>
                    my<b>calendar</b>
                </span>
            </div>
        </header>
        <main>
            <SwitchUser onUserSwitch={this.onUserSwitch}/>
            <Calendar onSelectDate={this.onSelectedDate}/>
            <Scheduler firstDay={this.state.startDay} userId={this.state.userId}/>
        </main>
      </div>
    );
  }
}

export default App;
