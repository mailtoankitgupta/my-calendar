import React from 'react';

class ScaleColumn extends React.Component {
	renderScaleCell(scaleInterval, index) {
		return (
			<div key={index} className="weekCalendar__scaleCell">
				<span>{scaleInterval.start.format("HH:mm")}</span>
			</div>
		);
	}


	render() {
		const {scaleIntervals} = this.props;
		return (
			<div>
				{scaleIntervals.map((scaleInterval, index) => this.renderScaleCell(scaleInterval, index))}
			</div>
		);
	}
}

export default ScaleColumn;