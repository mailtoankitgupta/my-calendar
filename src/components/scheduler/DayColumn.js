import React from 'react';
import DayCell from './DayCell';

class DayColumn extends React.Component {
	handleSelectionStart = (row) => {
		this.props.onSelectionStart(row);
	}

	handleCellMouseEnter = (row) => {
		this.props.onCellMouseEnter(row);
	}

	render() {
		const {date, dayIntervals} = this.props;
		const dayCells = dayIntervals.map((interval, rowPos) => (
			<div
				key={rowPos}
				className="calendarBody__cell"
				onMouseEnter={() => this.handleCellMouseEnter(rowPos)}>
				<DayCell
					rowPos={rowPos}
					startTime={interval.start}
					endTime={interval.end}
					startSelection={() => this.handleSelectionStart(rowPos)}
					/>
			</div>
		));

		return (
			<div>
				<div className="weekCalendar__headerWrapper">
					<span className="title">{date.format('ddd, DD')}</span>
				</div>
				<div className="calendarBody__column">{dayCells}</div>
			</div>
			
		);
	}
}

export default DayColumn;