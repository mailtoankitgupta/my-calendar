import React, {Component} from 'react';
import moment from 'moment';
import * as Utils from './Utils';
import $ from 'jquery';
import ScaleColumn from './DayScaleColumn';
import DayColumn from './DayColumn';
import Event from './Event';

class DayScheduler extends Component {
	constructor(props) {
		super(props);
		const {scaleUnit} = props;
		const scaleIntervals = Utils.getIntervalsByDuration(scaleUnit, moment({ h: 0, m: 0 }), moment({ h: 23, m: 59 }))

		this.state = {
			scaleIntervals,
			preselectedInterval: null
		};
	}

	closeModal = () => {
		this.setState({
			preselectedInterval: null
		});
	}

	handleSelectionStart = (row) => {
		const startSelectionPosition = {
			y: row,
		};

		this.setState({
			startSelectionPosition,
			mousePosition: startSelectionPosition,
		});
	}

	handleCellMouseEnter = (row) => {
		const {startSelectionPosition} = this.state;
		if (startSelectionPosition != null) {
			this.setState({
				mousePosition: {
					y: row,
				}
			});
		}
	}

	handleSelectionStop = (e) => {
		if (e.button == 0) {
			const {firstDay, scaleUnit} = this.props;
			const {startSelectionPosition, mousePosition, scaleIntervals} = this.state;

			if (startSelectionPosition == null)
				return;

			const endRow = mousePosition.y;
			const minCellIndex = Math.min(startSelectionPosition.y, endRow);
			const maxCellIndex = Math.max(startSelectionPosition.y, endRow)+1;

			const startSelectionTime = Utils.getMoment(scaleUnit, minCellIndex, 0);
			const endSelectionTime = Utils.getMoment(scaleUnit, maxCellIndex, 0);

			const start = moment(firstDay)
				.hour(startSelectionTime.hour())
				.minute(startSelectionTime.minute())
				.second(0);

			const end = moment(firstDay)
				.hour(endSelectionTime.hour())
				.minute(endSelectionTime.minute())
				.second(0);

			const preselectedInterval = {
				start,
				end,
			};

			console.log("selected interval: ", preselectedInterval);

			this.setState({
				preselectedInterval,
				updateEvent: false,
				startSelectionPosition: null,
				mousePosition: null,
			});
		}
	}

	componentDidMount() {
		window.addEventListener('mouseup', this.handleSelectionStop);
	}

	componentWillUnmount() {
		window.removeEventListener('mouseup', this.handleSelectionStop);
	}

	submitPreselectedInterval = (newValue) => {
		const {preselectedInterval} = this.state;
		this.props.onIntervalSelect([Object.assign(preselectedInterval, newValue)]);
		this.setState({
			preselectedInterval: null
		});
	}

	renderModal() {
		const { preselectedInterval } = this.state;
		const ModalComponent = this.props.modalComponent;

		if (preselectedInterval != null) {
			return (
				<div className="calendarModal">
					<div className="calendarModal__backdrop" onClick={this.closeModal}>
						<div className="calendarModal__content" onClick={(e) => e.stopPropagation()}>
							<ModalComponent
								{...preselectedInterval}
								onSave={this.submitPreselectedInterval} />
						</div>
					</div>
				</div>
			);
		}
	}

	renderSelectedIntervals() {
		const {scaleUnit, firstDay, selectedIntervals} = this.props;
		const {scaleIntervals} = this.state;

		const day = moment(firstDay).startOf('day');
		const intervals = selectedIntervals.filter(interval => interval.start.isSame(day, 'day') || 
			interval.end.isSame(day, 'day'));

		const cells = [];
		const result = [];
		if (intervals.length > 0) {
			intervals.sort((i1, i2) => i1.start.diff(i2.start, 'minutes'));

			intervals.forEach((interval, index, array) => {
				let startY = 0;
				if (!interval.start.isBefore(day)) {
					startY = Utils.getNumberOfCells(interval.start, scaleUnit, false, 0)+1;
				}

				if (startY > scaleIntervals.length) {
					return;
				}

				let endY = Utils.getNumberOfCells(interval.end, scaleUnit, true, 0)+1;
				if (endY > scaleIntervals.length) {
					endY = scaleIntervals.length;
				}

				const cellHeight = 25; // move to class level constant
				const eventSpacing = 50;

				const calendarPosition = $('.weekCalendar__content').position();

				const intersect = cells.filter((cell) => cell.startY < endY && cell.endY > startY).length;

				const width = eventSpacing;
				const gutter = eventSpacing/4;
				const height = (endY - startY + 1) * cellHeight;
				const top = startY * cellHeight + calendarPosition.top;
				const left = (width+gutter)*intersect + calendarPosition.left;

				const eventStyle = {
					top,
					left,
					width,
					height
				};

				const eventComponent = (
					<div
						className="weekCalendar__overlay"
						key={index}
						style={eventStyle}
						>
						<Event {...interval} />
					</div>
				);
				result.push(eventComponent);
				cells.push({
					startY,
					endY,
				});
			})
		}
		return result;
	}

	render() {
		const {firstDay} = this.props;
		const intervals = Utils.getDayIntervals(moment(firstDay), this.state.scaleIntervals);

		return (
			<div>
				<div className="weekCalendar__scaleColumn">
					<ScaleColumn scaleIntervals={this.state.scaleIntervals} />
				</div>
				<div className="weekCalendar__content">
					<div className="calendarBody" >
				        <div className="calendarBody__row">
				          <DayColumn 
				          	date={firstDay}
				          	dayIntervals={intervals}
				          	onSelectionStart={this.handleSelectionStart}
				          	onCellMouseEnter={this.handleCellMouseEnter}/>
				        </div>
				    </div>
				</div>
				{this.renderSelectedIntervals()}
				{this.renderModal()}
			</div>
		);
	}
}

export default DayScheduler;