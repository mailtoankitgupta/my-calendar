import React from 'react';

class Event extends React.Component {
	render() {
		const {start, end, users, value} = this.props;
		return (
			<div className="event">
				<span>{`${start.format('HH:mm')} - ${end.format('HH:mm')}`}</span>
				<br />
				{value}
				<div>
					{users.map(user => (<span>{user} </span>))}
				</div>
			</div>
		);
	}
}

export default Event;