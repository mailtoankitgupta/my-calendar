import React from 'react';

class DayCell extends React.Component {
	handleMouseDown = (e) => {
		if (e.button == 0) {
			this.props.startSelection();
		}
	}


	render() {
		return (
			<div className="dayCell" onMouseDown={this.handleMouseDown}>&nbsp;</div>
		);
	}
}

export default DayCell;