import React from 'react';
import PropTypes from 'prop-types';
import $ from 'jquery';
import SchedulerService from '../services/SchedulerService';

const propTypes = {
  start: PropTypes.object.isRequired,
  end: PropTypes.object.isRequired,
  value: PropTypes.string,
  onRemove: PropTypes.func,
  onSave: PropTypes.func.isRequired,
  actionType: PropTypes.string, // eslint-disable-line react/no-unused-prop-types
};

const defaultProps = {
  value: '',
};


class MeetingModal extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      users: ['user1', 'user2', 'user3', 'user4', 'user5'],
      selected: [],
      msg: ''
    };
    this.schedulerService = new SchedulerService();
  }

  handleRemove = () => {
    this.props.onRemove();
  }

  handleSave = () => {
    const { value } = this.input;
    this.props.onSave({
      'data': JSON.stringify({
        'value': value,
        'users': this.state.selected
      })
    });
  }

  checkSchedule(schedule) {
    this.schedulerService.checkSchedule(schedule)
        .then(
          (result) => {
            // TODO: suggest the users and their conflicting schedules            
            this.setState({
              msg: result.is_meeting_conflict ? 'this meeting conflicts with the users' : 'no conflict'  
            });
          },
          (error) => {
                this.setState({
                  msg: 'error in checking...',
                  error
                });
              }
        )
  }

  onChange = (event) => {
    const {...schedule} = this.props;
    console.log(schedule);
    const users = $(event.target).val();
    this.setState({
      selected: users
    })
    this.checkSchedule(Object.assign({'users': users}, schedule));
  }

  renderUsers() {
    const {users} = this.state;
    const options = users.map(user => {
      return <option key={user} value={user}>{user}</option>;
    });

    return (
      <select onChange={this.onChange} multiple>{options}</select>
    )
  }

  renderText() {
    const {
      start,
      end,
    } = this.props;

    if (start.isSame(end, 'day')) {
      return (<span>{`${start.format('Do MMM., HH:mm')} - ${end.format('HH:mm')}`}</span>);
    }
    return (<span>{`${start.format('Do MMM.')} - ${end.format('Do MMM.')}, ${start.format('HH:mm')} - ${end.format('HH:mm')}`}</span>);
  }

  render() {
    const {
      value,
    } = this.props;
    return (
      <div className="customModal">
        <div className="customModal__text">{this.renderText()}</div>
        {this.renderUsers()}
        <input
          ref={(el) => { this.input = el; }}
          className="customModal__input"
          type="text"
          placeholder="Enter details"
          defaultValue={value}
        />
        <span>{this.state.msg}</span>
        <button className="customModal__button" onClick={this.handleRemove}>Delete</button>
        <button className="customModal__button customModal__button_float_right" onClick={this.handleSave}>Save</button>
      </div>
    );
  }
}

MeetingModal.propTypes = propTypes;
MeetingModal.defaultProps = defaultProps;
export default MeetingModal;