import React from 'react';
import moment from 'moment';
import 'react-week-calendar/dist/style.css';
import MeetingModal from './MeetingModal';
import SchedulerService from '../services/SchedulerService';
import DayScheduler from './scheduler/DayScheduler';

export default class Scheduler extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			selectedIntervals: [],
			msg: 'fetching...'
		};
		this.handleSelect = this.handleSelect.bind(this);
		this.schedulerService = new SchedulerService();
	}

	componentDidUpdate(prevProps) {
		if (this.props.userId !== prevProps.userId) {
			console.log("changed user: ", this.props, prevProps);
			this.schedulerService.fetchData(this.props.userId)
				.then(
					(result) => {
						let schedules = result.map((schedule) => {
							schedule.start = moment(schedule.start);
							schedule.end = moment(schedule.end);
							return schedule;
						})
						this.setState({
							selectedIntervals: schedules,
							msg: 'load successful'
						});
					},
					(error) => {
			          this.setState({
			            msg: 'load failed',
			            error
			          });
			        }
				)
		}
	}

	componentDidMount() {
		this.schedulerService.fetchData(this.props.userId)
			.then(
				(result) => {
					let schedules = result.map((schedule) => {
						schedule.start = moment(schedule.start);
						schedule.end = moment(schedule.end);
						return schedule;
					})
					this.setState({
						selectedIntervals: schedules,
						msg: 'load successful'
					});
				},
				(error) => {
		          this.setState({
		            msg: 'load failed',
		            error
		          });
		        }
			)
	}

	handleEventRemove = (event) => {
		// event.uid
		// TODO
	}

	handleSelect = (newIntervals) => {
		const {selectedIntervals} = this.state;
		const data = JSON.parse(newIntervals[0].data);
		const newSchedule = {
			start: newIntervals[0].start,
			end: newIntervals[0].end,
			value: data.value,
			users: data.users
		};
		console.log("saving", newSchedule);
		this.schedulerService.saveData(this.props.userId, newSchedule)
  			.then(
  				(result) => {
  					let schedule = result.schedule;
  					schedule.start = moment(schedule.start);
  					schedule.end = moment(schedule.end);
  					this.setState({
  						msg: 'saved...',
  						selectedIntervals: selectedIntervals.concat(schedule)
  					});
  				},
  				(error) => {
  					this.setState({
  						msg: 'error in saving...',
  						error
  					});
  				}
  			);
	}

	render() {
		return (
			<div className="week-calendar-container">
	            <div className="calendar__currentUser">Showing calendar for user: {this.props.userId}</div>
				<DayScheduler
	              firstDay={this.props.firstDay}
	              scaleUnit={60}
	              selectedIntervals={this.state.selectedIntervals}
	              onIntervalSelect={this.handleSelect}
	              onIntervalRemove={this.handleRemove}
	              modalComponent={MeetingModal}
	              />
	            <div>{this.state.msg}</div>
	        </div>   
		);
	}	
};