import React, { Component } from 'react';

class SwitchUser extends Component {
  constructor(props) {
    super(props);
    this.state = {
    	users: ['user1', 'user2', 'user3', 'user4', 'user5']
    };
  }

  componentDidMount() {
  	// fetch from API
  }

  onChange = (event) => {
  	this.props.onUserSwitch(event.target.value);
  }

  render() {
  	const {users} = this.state;
  	const options = users.map(user => {
  		return <option key={user} value={user}>{user}</option>;
  	});

  	return (
  		<div className="switch-user">
  			<select onChange={this.onChange}>{options}</select>
  		</div>
  	)
  }
}

export default SwitchUser;