class SchedulerService {

	getScheduleAPI(userId) {
		return "http://localhost:5000/api/v1/users/" + userId + "/schedule";
	}

	fetchData(userId) {
		return fetch(this.getScheduleAPI(userId), {
				mode: 'cors',
	  			headers: {
	    			'Access-Control-Allow-Origin':'*'
	  			}})
				.then(res => res.json());
	}

	saveData(userId, newSchedule) {
		return fetch(this.getScheduleAPI(userId), {
			method: 'POST',
			mode: 'cors',
			body: JSON.stringify(newSchedule),
  			headers: {
    			'Access-Control-Allow-Origin':'*',
    			'Content-Type': 'application/json'
  			}})
  			.then(res => res.json());
	}

	checkSchedule(schedule) {
		return fetch("http://localhost:5000/api/v1/schedule/check", {
	        method: 'POST',
	        mode: 'cors',
	        body: JSON.stringify(schedule),
  			headers: {
    			'Access-Control-Allow-Origin':'*',
    			'Content-Type': 'application/json'
  			}})
	        .then(res => res.json());
	}
}

export default SchedulerService;